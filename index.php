<?php
# step 1
// catch event
if (isset($_POST['submit'])) {
    $errors = [];
    # step 2
    // validations
    if (empty($_POST['name'])) {
        $errors[] = 'Name is required';
    }

    if (empty($_POST['degree'])) {
        $errors[] = 'Degree is required';
    }

    #step 3
    // logic or business
    if (count($errors) === 0) {
        $result = null;
        $alert = null;
        $degree = $_POST['degree'];
        if ($degree >= 90) {
            $result = 'A';
            $alert = 'success';
        } elseif ($degree >= 80) {
            $result = 'B';
            $alert = 'info';
        } elseif ($degree >= 70) {
            $result = 'C';
            $alert = 'primary';
        } elseif ($degree >= 60) {
            $result = 'D';
            $alert = 'warning';
        } elseif ($degree >= 50) {
            $result = 'E';
            $alert = 'secondary';
        } else {
            $result = 'F';
            $alert = 'danger';
        }
    }
    


}
?>
<!doctype html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
    </head>

    <body>
        <div class="container mt-5">
            <div class="row">
                <div class="col-12">
                    <?php
                    for ($i = 0; isset($errors) && $i < count($errors); $i++) :
                    ?>
                    <div class="alert alert-danger">
                        <?= $errors[$i] ?>
                    </div>
                    <?php endfor; ?>
                </div>
                <div class="col-4">
                    <form method="post">
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                        <div class="form-group">
                            <label for="degree">Degree</label>
                            <input type="number" class="form-control" id="degree" name="degree">
                        </div>
                        <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
                <div class="col-4 d-flex justify-content-center align-items-center">
                    <?php
                    if (isset($result)) :
                    ?>
                    <h3 class="alert p-4 alert-<?= $alert . '"'?>>
                        <?= $result ?>
                    </h3>
                    <?php endif; ?>
                </div>

            </div>
        </div>
        <script src=" js/jquery-3.4.1.min.js"> </script> <script src="js/bootstrap.min.js">
                        </script>
    </body>

</html>